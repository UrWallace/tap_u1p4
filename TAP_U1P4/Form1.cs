﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TAP_U1P4
{
    public partial class Form1 : Form
    {
        private List<String> asientos;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            asientos = new List<string>();
            try
            {
                FileInfo fi = new FileInfo("asientosDisponibles.txt");
                if (fi.Exists)
                {
                    leerArchivo();
                }
                else
                {
                    crearArchivo();
                }
                // Agregarlos al combobox
                foreach(String a in asientos)
                {
                    comboBoxAsientos.Items.Add(a);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error al leer el archivo.\n"+ex);
            }
        }

        private void crearArchivo()
        {
            
            for (int i = 0; i < 30; i++)
            {
                if((i+1) <= 10)
                {
                    asientos.Add("A" + (i + 1));
                }else if ((i+1)<=20)
                {
                    asientos.Add("B" + (i + 1));
                }
                else
                {
                    asientos.Add("C" + (i + 1));
                }
                //Console.WriteLine(asientos[i]);
            }
            try
            {
                StreamWriter sw = new StreamWriter("asientosDisponibles.txt");
                foreach(String asiento in asientos)
                {
                    sw.WriteLine("asiento");
                }
                sw.Close();
            }
            catch (Exception ex)
            {

                Console.WriteLine("Error al crear el archivo.\n"+ex);
            }
        }

        private void leerArchivo()
        {
            asientos.Clear();
            try
            {
                StreamReader sr = new StreamReader("asientosDisponibles.txt");
                String lectura = null;
                while ((lectura = sr.ReadLine()) != null)
                {
                    asientos.Add(lectura);
                }
                sr.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error al leer.\n"+ex);
            }
        }

        private void actualizaCombo()
        {
            comboBoxAsientos.Items.Clear();
            foreach (String a in asientos)
            {
                comboBoxAsientos.Items.Add(a);
            }
        }

        private void numericUpDownBoletos_ValueChanged(object sender, EventArgs e)
        {
            if(numericUpDownBoletos.Value > 0)
            {
                buttonGuardar.Enabled = true;
            }
            else
            {
                buttonGuardar.Enabled = false;
            }
        }

        private void buttonGuardar_Click(object sender, EventArgs e)
        {
            if(listBoxSeleccion.Items.Count < numericUpDownBoletos.Value)
            {
                listBoxSeleccion.Items.Add(comboBoxAsientos.SelectedItem.ToString());
                asientos.RemoveAt(comboBoxAsientos.SelectedIndex);
                actualizaCombo();
                buttonEliminar.Enabled = true;
            }
            else
            {
                buttonGuardar.Enabled = false;
            }
            
        }

        private void buttonEliminar_Click(object sender, EventArgs e)
        {
            asientos.Add(listBoxSeleccion.SelectedItem.ToString());
            listBoxSeleccion.Items.RemoveAt(listBoxSeleccion.SelectedIndex);
            asientos.Sort();
            actualizaCombo();
        }

        private void buttonFinalizar_Click(object sender, EventArgs e)
        {
            //Se guarda en archivo de ventas
            try
            {
                StreamWriter sw = new StreamWriter("ventas.txt",true);
                foreach (object o in listBoxSeleccion.Items)
                {
                    sw.WriteLine(o.ToString());
                }
                sw.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error al crear la venta.\n"+ex);
            }
            //Se sobreescribe el archivo de asientos disponibles
            try
            {
                StreamWriter sw = new StreamWriter("asientosDisponibles.txt",false);
                foreach (String a in comboBoxAsientos.Items)
                {
                    sw.WriteLine(a);
                }
                sw.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error.\n"+ex);
            }
            MessageBox.Show("Venta realizada");
            Close();
        }
    }
}
